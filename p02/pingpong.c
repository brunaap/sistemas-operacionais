#include <stdio.h>
#include <stdlib.h>
#include "pingpong.h"

// funções gerais ==============================================================
//#define DEBUG;
#define STACKSIZE 32768		/* tamanho de pilha das threads */
int id_task;
int id_atual;
task_t *atual;
task_t main_t;
ucontext_t main_c;

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init()
{
	id_task = 0;
	id_atual = 0;
    
    main_t.context = &main_c;
    main_t.id = id_task;
    id_task=1;
	atual = &main_t;
	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf(stdout, 0, _IONBF, 0);
}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create(task_t *task,			// descritor da nova tarefa
	void(*start_func)(void *),	// funcao corpo da tarefa
	void *arg)			// argumentos para a tarefa
{
	ucontext_t* context = malloc(sizeof(ucontext_t));
	task->context = context;
	char *stack = NULL;

	getcontext(context);

	stack = malloc(STACKSIZE);
	if (stack)
	{
		context->uc_stack.ss_sp = stack;
		context->uc_stack.ss_size = STACKSIZE;
		context->uc_stack.ss_flags = 0;
		context->uc_link = 0;
	}
	else
	{
		perror("Erro na criação da pilha: ");
		return(-1);
	}

	makecontext(context, (void*)(*start_func), 1, arg);

	task->id = id_task;
    #ifdef DEBUG
    printf("Criou tarefa %d \n", id_task);
    #endif
	id_task += 1;

	return(id_task);
	
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit(int exitCode)
{
	task_switch(&main_t);
}

// alterna a execução para a tarefa indicada
int task_switch(task_t *task)
{
    
    if(atual != NULL && task!=NULL && atual->context != NULL && task->context!=NULL)
    {
        #ifdef DEBUG
        printf("Mudar de tarefa %d para tarefa %d \n",atual->id, task->id); 
        #endif
        ucontext_t* origem;
        origem = atual->context;
        atual = task;
        id_atual = task->id;
        swapcontext(origem, task->context);
        
    }
    else
	{
        return (-1);
	}
    
    return 0;
}

// retorna o identificador da tarefa corrente (main eh 0)
int task_id()
{
	return(id_atual);
}

// suspende uma tarefa, retirando-a de sua fila atual, adicionando-a à fila
// queue e mudando seu estado para "suspensa"; usa a tarefa atual se task==NULL
void task_suspend(task_t *task, task_t **queue);

// acorda uma tarefa, retirando-a de sua fila atual, adicionando-a à fila de
// tarefas prontas ("ready queue") e mudando seu estado para "pronta"
void task_resume(task_t *task);