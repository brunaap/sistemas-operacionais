#include "pingpong.h"

// funções gerais ==============================================================
//#define DEBUG
#define STACKSIZE 32768 /* tamanho de pilha das threads */
int id_task;
int id_atual;

int userTasks;
int prioridade_max;
int task_aging = 1;

int quantum_atual = 0;
int quantum = 18;
int ticks = 0;

task_t *atual;
task_t main_t;
task_t task_dispatcher;
queue_t *fila_prontas;
ucontext_t main_c;

struct sigaction action;
// estrutura de inicialização to timer
struct itimerval timer;

void ticks_handler()
{
	ticks++;
	if (atual->user_task)
	{
		quantum_atual = quantum_atual - 1;
		//printf("%d",quantum_atual);
		if (quantum_atual <= 0)
			task_yield();
	}
}

// decide a proxima tareda que sera ativada. utiliza prioridades
task_t *scheduler()
{
	if (fila_prontas)
	{
		task_t *executar_agora = (task_t *)fila_prontas;
		task_t *aux = (task_t *)fila_prontas;

		prioridade_max = task_getprio(aux);

		for (aux = aux->next; aux != (task_t *)fila_prontas; aux = aux->next)
		{
			if (prioridade_max > task_getprio(aux))
			{
				prioridade_max = task_getprio(aux);
			}
		}
		#ifdef DEBUG
				printf("Prioridade máxima: %d,Tarefa: %d, Prioridade da tarefa: %d \n", prioridade_max, executar_agora->id, task_getprio(executar_agora));
		#endif
		while (task_getprio(executar_agora) > prioridade_max)
		{
			queue_remove((queue_t **)&fila_prontas, (queue_t *)executar_agora); // tirando da fila
			queue_append((queue_t **)&fila_prontas, (queue_t *)executar_agora);
			executar_agora = (task_t *)fila_prontas;
		}
		//printf("Tarefa a ser feita: %d \n",executar_agora->id);
		return executar_agora;
	}
	else
		return NULL;
}

// corpo do dispatcher
void dispatcher_body()
{
	userTasks = (int)queue_size((queue_t *)fila_prontas); // pega o numero de tarefas prontas
	task_t *next = NULL;
	while (userTasks > 0) // enquanto tiverem tarefas prontas na fila
	{
		next = scheduler(); // scheduler é uma função
		if (next)
		{
			task_switch(next); // transfere controle para a tarefa "next"
		}
	}
	task_exit(0); // encerra a tarefa dispatcher
}

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init()
{
	id_task = 0;
	id_atual = 0;
	prioridade_max = 20; // THE END

	//TIMER -----------------------------------------------------------------------------------------
	action.sa_handler = ticks_handler;
	sigemptyset(&action.sa_mask);
	action.sa_flags = 0;

	if (sigaction(SIGALRM, &action, 0) < 0)
	{
		perror("Erro em sigaction: ");
		exit(1);
	}
	timer.it_value.tv_usec = 1;		  // primeiro disparo, em micro-segundos
	timer.it_value.tv_sec = 0;		  // primeiro disparo, em segundos
	timer.it_interval.tv_usec = 1000; // disparos subsequentes, em micro-segundos
	timer.it_interval.tv_sec = 0;	 // disparos subsequentes, em segundos
	//END TIMER -----------------------------------------------------------------------------------------
	main_t.context = &main_c;
	main_t.id = id_task;
	main_t.user_task = 1;
	id_task = 1;
	atual = &main_t;

	task_create(&task_dispatcher, (void *)(dispatcher_body), "dispatcher"); // criando dispatcher
	id_task = 2;
	task_dispatcher.user_task = 0;
	quantum_atual = quantum;
	if (setitimer(ITIMER_REAL, &timer, 0) < 0)
	{
		perror("Erro em setitimer: ");
		exit(1);
	}
	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf(stdout, 0, _IONBF, 0);
}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create(task_t *task,				// descritor da nova tarefa
				void (*start_func)(void *), // funcao corpo da tarefa
				void *arg)					// argumentos para a tarefa
{
	ucontext_t *context = malloc(sizeof(ucontext_t));
	task->user_task = 1;
	task->context = context;
	char *stack = NULL;

	getcontext(context);

	stack = malloc(STACKSIZE);
	if (stack)
	{
		context->uc_stack.ss_sp = stack;
		context->uc_stack.ss_size = STACKSIZE;
		context->uc_stack.ss_flags = 0;
		context->uc_link = 0;
	}
	else
	{
		perror("Erro na criação da pilha: ");
		return (-1);
	}

	makecontext(context, (void *)(*start_func), 1, arg);

	task->id = id_task;
	#ifdef DEBUG
		printf("Criou tarefa %d \n", id_task);
	#endif

	if (task->id > 1)
	{
		queue_append((queue_t **)&fila_prontas, (queue_t *)(task));
	}
	id_task += 1;
	return (id_task);
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit(int exitCode)
{
	userTasks--;
	if (fila_prontas)
		queue_remove(&fila_prontas, (queue_t *)atual);
	if (id_atual == 1) // ta no dispatcher
		task_switch(&main_t);
	else // está na main
		task_switch(&task_dispatcher);
}

// alterna a execução para a tarefa indicada
int task_switch(task_t *task)
{

	if (atual != NULL && task != NULL && atual->context != NULL && task->context != NULL)
	{
	#ifdef DEBUG
			printf("Mudar de tarefa %d para tarefa %d \n", atual->id, task->id);
	#endif
		ucontext_t *origem;
		origem = atual->context;
		atual = task;
		id_atual = task->id;
		quantum_atual = quantum;
		swapcontext(origem, task->context);
	}
	else
	{
		return (-1);
	}

	return 0;
}

// permite a uma tarefa voltar ao final da fila de prontas
void task_yield()
{
	quantum_atual = quantum;
	if (id_atual != 0)
	{
		task_t *executar_agora = (task_t *)fila_prontas;
		executar_agora->priority = executar_agora->priority + task_aging;
	}
	task_switch(&task_dispatcher);
}

// retorna o identificador da tarefa corrente (main eh 0)
int task_id()
{
	return (id_atual);
}

void task_setprio(task_t *task, int prio)
{
	if (task != NULL)
	{
		if (prio >= -20 && prio <= 20)
		{
			task->priority = prio;
			if (prio < prioridade_max)
				prioridade_max = prio;
		}
		else
		{
			printf("Valor de prioridade não aceito");
		}
	}
	else
	{
		atual->priority = prio;
	}
}

int task_getprio(task_t *task)
{
	if (task != NULL)
		return (task->priority);
	else
		return (atual->priority);
}

// suspende uma tarefa, retirando-a de sua fila atual, adicionando-a à fila
// queue e mudando seu estado para "suspensa"; usa a tarefa atual se task==NULL
void task_suspend(task_t *task, task_t **queue);

// acorda uma tarefa, retirando-a de sua fila atual, adicionando-a à fila de
// tarefas prontas ("ready queue") e mudando seu estado para "pronta"
void task_resume(task_t *task);
