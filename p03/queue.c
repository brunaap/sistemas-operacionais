//------------------------------------------------------------------------------
// Definição e operações em uma fila genérica.
// by Carlos Maziero, DAINF/UTFPR, maio/2013
//------------------------------------------------------------------------------

#ifndef __QUEUE__
#define __QUEUE__

#ifndef NULL
#define NULL ((void *) 0)
#endif

#include <stdio.h>

//------------------------------------------------------------------------------
// estrutura de uma fila genérica, sem conteúdo definido.
// Veja um exemplo de uso desta estrutura em testafila.c

typedef struct queue_t
{
   struct queue_t *prev ;  // aponta para o elemento anterior na fila
   struct queue_t *next ;  // aponta para o elemento seguinte na fila
} queue_t ;

//------------------------------------------------------------------------------
// Insere um elemento no final da fila.
// Condicoes a verificar, gerando msgs de erro:
// - a fila deve existir
// - o elemento deve existir
// - o elemento nao deve estar em outra fila ------------------------------------------------------------------------------------------------------------------ ###

//lembre-se que a fila eh duplamente encadeada!
// queue e um ponteiro para um ponteiro que aponta para o primeiro elemento da
// fila e elem eh um ponteiro para o elemento a ser inserido
void queue_append (queue_t **queue, queue_t *elem)
{
   if( elem != NULL && queue != NULL && elem->next==NULL ) // verifica se o elemento que queremos inserir eh nulo
   {
      if( *queue == NULL ) // a fila esta vazia
      {
         *queue=elem;
         elem->next=elem;
         elem->prev=elem;
      }
      else // a fila nao esta vazia
      {
         queue_t* ult_elem = (*queue)->prev; // fila duplamente encadeada
         (*queue)->prev=elem;
         elem->next=(*queue); // recebe o primeiro elemento
         ult_elem->next=elem;
         elem->prev=ult_elem;
      }
   }
   if( elem == NULL )
      printf("Desculpe, o elemento nao existe.\n");
   if( queue == NULL )
      printf("Desculpe, o a fila nao existe.\n");
}

//------------------------------------------------------------------------------
// Remove o elemento indicado da fila, sem o destruir.
// Condicoes a verificar, gerando msgs de erro:
// - a fila deve existir
// - a fila nao deve estar vazia
// - o elemento deve existir
// - o elemento deve pertencer a fila indicada
// Retorno: apontador para o elemento removido, ou NULL se erro

queue_t *queue_remove (queue_t **queue, queue_t *elem)
{
   if( elem != NULL && queue != NULL ) // elemento nao nulo e fila nao vazia
   {
      queue_t* aux = *queue;
      int elemento_na_fila = 0;
      // verifica se o elemento está na fila
      if( elem==(*queue) )
         elemento_na_fila=1;
      else
      {
         for( aux=aux ;  aux->next!=(*queue) ; aux=aux->next )
         {
            if( (aux->next)==elem )
               elemento_na_fila=1; // o elemento esta na fila
         }
      }
      // remove o elemento da fila
      if(elemento_na_fila)
      {
         if( (*queue)->prev==(*queue) ) // apenas um elmento na fila
            *queue=NULL;
         else // mais de um elemento na fila
         {
            (elem->prev)->next=elem->next;
            (elem->next)->prev=elem->prev;
            if( elem==(*queue) ) // se o primeiro elemento for removido
               *queue=(*queue)->next;
         }
         elem->next=NULL;
         elem->prev=NULL;
         return(elem);
      }
      else
      {
         printf("Desculpe, o elemento nao pertence a essa fila.\n");
         return(NULL);
      }
   }
   else
   {
      if( elem == NULL )
         printf("Desculpe, o elemento nao existe.\n");
      if( queue == NULL )
         printf("Desculpe, o a fila nao existe.\n");
      return(NULL);
   }
}

//------------------------------------------------------------------------------
// Conta o numero de elementos na fila
// Retorno: numero de elementos na fila

int queue_size (queue_t *queue)
{
   if( queue == NULL ) // a fila esta vazia
   {
      return(0);
   }
   queue_t* elem_atual=queue; // fila duplamente encadeada
   int i;
   for( i=1 ; elem_atual->next!=queue ; elem_atual=elem_atual->next,i++ );
   return(i);
}

//------------------------------------------------------------------------------
// Percorre a fila e imprime na tela seu conteúdo. A impressão de cada
// elemento é feita por uma função externa, definida pelo programa que
// usa a biblioteca. Essa função deve ter o seguinte protótipo:
//
// void print_elem (void *ptr) ; // ptr aponta para o elemento a imprimir

void queue_print (char *name, queue_t *queue, void print_elem (void* ptr) )
{
    printf("%s", name);
    printf("[");
    if (queue==NULL) // a fila esta vazia
    {
        print_elem(NULL);
    }
    else{
        queue_t* elem_atual = queue; //inicio da fila encadeada
        do
        {
            print_elem(elem_atual);
            elem_atual=elem_atual->next;//proximo elemento
            printf(" ");
        }while (elem_atual!=queue);// para cada elemento da fila chama a funcao print_elem
    }
    printf("]\n");
}


#endif
