#include "pingpong.h"
#include <strings.h>

// funções gerais ==============================================================
//#define DEBUG
#define STACKSIZE 32768 /* tamanho de pilha das threads */
int id_task;
int id_atual;

int userTasks;
int sleepTasks;
int prioridade_max;
int task_aging = 1;

int quantum_atual = 0;
int quantum = 20;
int ticks = 0;
int zona_crit = 0;

int exit_Code = -10; 

task_t *atual;
task_t main_t;
task_t task_dispatcher; 
queue_t *fila_prontas = NULL;              // fila de tarefas printas
queue_t *fila_suspensas = NULL;            // fila de tarefas suspensaas
queue_t *fila_adormecidas = NULL;          // fila de tarefas adormecidas

semaphore_t sem_access;
semaphore_t sem_buffer;
struct sigaction action;
// estrutura de inicialização to timer
struct itimerval timer;


//informar às tarefas o valor corrente do relógio, numero de ticks decorridos desde a inicialização do programa (1 tick = 1ms)
unsigned int systime ()
{
	return ticks;
}


void ticks_handler()
{
	ticks++;
	if (atual->user_task && zona_crit == 0)
	{
		quantum_atual = quantum_atual - 1;
		if (quantum_atual <= 0)
			task_yield();
	}
	else
		quantum_atual = quantum;
}

// decide a proxima tareda que sera ativada. utiliza prioridades
task_t *scheduler()
{
	if (fila_prontas)
	{
		task_t *executar_agora = (task_t *)fila_prontas;
		task_t *aux = (task_t *)fila_prontas;

		prioridade_max = task_getprio(aux);

		for (aux = aux->next; aux != (task_t *)fila_prontas; aux = aux->next)
		{
			if (prioridade_max > task_getprio(aux))
			{
				prioridade_max = task_getprio(aux);
			}
		}
		#ifdef DEBUG
				printf("Prioridade máxima: %d,Tarefa: %d, Prioridade da tarefa: %d \n", prioridade_max, executar_agora->id, task_getprio(executar_agora));
		#endif
		while (task_getprio(executar_agora) > prioridade_max)
		{
			queue_remove((queue_t **)&fila_prontas, (queue_t *)executar_agora); // tirando da fila
			queue_append((queue_t **)&fila_prontas, (queue_t *)executar_agora);
			executar_agora = (task_t *)fila_prontas;
		}
		return executar_agora;
	}
	else
		return NULL;
}

// corpo do dispatcher
void dispatcher_body()
{
    // percorrendo a fila de prontas e a organizando
	userTasks = (int)queue_size((queue_t *)fila_prontas); // pega o numero de tarefas prontas
	task_t *next = NULL;
	while (userTasks > 0)
	{
        // percorrendo a fila de tarefas adormecidas e acordando as que o tempo estorou
		if( fila_adormecidas )
		{
			task_t *tarefa_sleep = (task_t *)fila_adormecidas;
			task_t *tarefa_first = (task_t *)fila_adormecidas;
			int time = systime();
			//sleepTasks = (int)queue_size((queue_t *)fila_adormecidas);
			do
			{
				task_t *temp = tarefa_sleep->next;
			//	printf("time: %d\n",time);
				if ( tarefa_sleep->task_wake <= time ) // a tarefa deveser acordada
				{
					
					// removendo da fila de adormecidas e colocando na fila de prontas
					queue_remove((queue_t **)&fila_adormecidas, (queue_t *)(tarefa_sleep)); 
					queue_append((queue_t **)&fila_prontas, (queue_t *)(tarefa_sleep));
					
					tarefa_first = (task_t *)fila_adormecidas;
					//sleepTasks--;
				}
				
				tarefa_sleep = temp;
			} while (tarefa_sleep != NULL && tarefa_sleep != tarefa_first && tarefa_first != NULL);
			
		}
     		
        // voltando às funções usuais do dispatcher
		next = scheduler(); // scheduler é uma função
		if (next)
		{
			next->tempo_entrou_na_tarefa = systime(); // tafeva vai ser executada, é preciso computadar o tempo de processamento dela
			task_switch(next); // transfere controle para a tarefa "next"
		}
	}
  
	task_exit(0); // encerra a tarefa dispatcher
}

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init()
{
	id_task = 0;
	id_atual = 0;
	prioridade_max = 20; // THE END
	main_t.context = malloc(sizeof(ucontext_t));
	char *stack = NULL;

	getcontext(main_t.context);

	stack = malloc(STACKSIZE);
	if (stack)
	{
		main_t.context->uc_stack.ss_sp = stack;
		main_t.context->uc_stack.ss_size = STACKSIZE;
		main_t.context->uc_stack.ss_flags = 0;
		main_t.context->uc_link = 0;
	}
	else
	{
		perror("Erro na criação da pilha: ");
	}


	//TIMER -----------------------------------------------------------------------------------------
	action.sa_handler = ticks_handler;
	sigemptyset(&action.sa_mask);
	action.sa_flags = 0;

	if (sigaction(SIGALRM, &action, 0) < 0)
	{
		perror("Erro em sigaction: ");
		exit(1);
	}
	timer.it_value.tv_usec = 1;		  // primeiro disparo, em micro-segundos
	timer.it_value.tv_sec = 0;		  // primeiro disparo, em segundos
	timer.it_interval.tv_usec = 1; // disparos subsequentes, em micro-segundos
	timer.it_interval.tv_sec = 0;	 // disparos subsequentes, em segundos
	//END TIMER -----------------------------------------------------------------------------------------

	id_task = 1;
	task_create(&task_dispatcher, (void *)(dispatcher_body), "dispatcher"); // criando dispatcher
	id_task = 0;
	task_dispatcher.user_task = 0;
	quantum_atual = quantum;
	if (setitimer(ITIMER_REAL, &timer, 0) < 0)
	{
		perror("Erro em setitimer: ");
		exit(1);
	}
	
	//INICIAR A MAIN --------------------------------------------------------
	main_t.id = id_task;
	main_t.user_task = 1;
	id_task = 2;
	atual = &main_t;
	queue_append((queue_t**)&fila_prontas, (queue_t*)(&main_t));
	main_t.tempo_inicio = systime();
	main_t.tempo_execucao = 0;
  	main_t.tempo_processador = 0;
  	main_t.activations = 0;
	main_t.tempo_entrou_na_tarefa = 0;

	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf(stdout, 0, _IONBF, 0);
}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID>0 ou erro.
int task_create(task_t *task,				// descritor da nova tarefa
				void (*start_func)(void *), // funcao corpo da tarefa
				void *arg)					// argumentos para a tarefa
{
	ucontext_t *context = malloc(sizeof(ucontext_t));
	task->user_task = 1;
	task->context = context;
	char *stack = NULL;

	getcontext(context);

	stack = malloc(STACKSIZE);
	if (stack)
	{
		context->uc_stack.ss_sp = stack;
		context->uc_stack.ss_size = STACKSIZE;
		context->uc_stack.ss_flags = 0;
		context->uc_link = 0;
	}
	else
	{
		perror("Erro na criação da pilha: ");
		return (-1);
	}

	makecontext(context, (void *)(*start_func), 1, arg);

	task->id = id_task;
	#ifdef DEBUG
		printf("Criou tarefa %d \n", id_task);
	#endif

	if (task->id > 1)
	{
		queue_append((queue_t **)&fila_prontas, (queue_t *)(task));
	}
	id_task += 1;
        task->task_wake = -10;  // a tarefa ainda não está dormindo
	task->task_suspend = -10; // tarefa que deve terminar para ela voltar
	task->pronta = 1;
	task->tempo_inicio = systime();
	task->tempo_execucao = 0;
  	task->tempo_processador = 0;
  	task->activations = 0;
	task->tempo_entrou_na_tarefa = 0;
	return (id_task);
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit(int exitCode)
{
	while(atual->id == 0 && (int) queue_size(fila_prontas) >1)
		task_yield();
	zona_crit= 1;
	exit_Code = exitCode;
	//tarefa saidno, retirna o tempo de execução dela
	atual->tempo_execucao = systime() - (atual->tempo_inicio);
	printf("Task %d exit: execution time %d ms, processor time %d ms, %d activations\n", atual->id, atual->tempo_execucao, atual->tempo_processador, atual->activations);
	userTasks--;
	
	if (fila_prontas)
		queue_remove(&fila_prontas, (queue_t *)atual);
	if (id_atual == 1) // ta no dispatcher
	{
		zona_crit = 0;
		task_switch(&main_t);
	}
	else // pode estar em qualquer uma menos a dispatcher
	{
		if( atual->id == 0 ) // está na main
		{
			zona_crit = 0;
			task_yield();
		}
		else // não está na main
		{
			zona_crit =0;
			task_switch(&main_t);
		}
	}
		
}

// alterna a execução para a tarefa indicada
int task_switch(task_t *task)
{
	if (atual != NULL && task != NULL && atual->context != NULL && task->context != NULL)
	{
		#ifdef DEBUG
			printf("Mudar de tarefa %d para tarefa %d \n", atual->id, task->id);
		#endif
		ucontext_t *origem;
		origem = atual->context;
		atual = task;
		id_atual = task->id;
		quantum_atual = quantum;
		swapcontext(origem, task->context);
	//	printf("Saiu de boa\n");
		return(0);
	}
	return (-1);
}

// permite a uma tarefa voltar ao final da fila de prontas
void task_yield()
{
	zona_crit = 1;
	// se entra aqui vai para o dispatcher
	(&task_dispatcher)->activations++;

	atual->tempo_processador+=(systime())-(atual->tempo_entrou_na_tarefa); // adicionando o tempo de uso do processador
	(atual->activations)++; // se voltou para a fila de prontas é porque foi ativada uma vez
	quantum_atual = quantum;
	task_t *executar_agora = (task_t *)fila_prontas;
	if(executar_agora!=NULL && executar_agora->priority>-20)
		executar_agora->priority = executar_agora->priority + task_aging;
	zona_crit =0;
	task_switch(&task_dispatcher);
}

// retorna o identificador da tarefa corrente (main eh 0)
int task_id()
{
	return (id_atual);
}

void task_setprio(task_t *task, int prio)
{
	if (task != NULL)
	{
		if (prio >= -20 && prio <= 20)
		{
			task->priority = prio;
			if (prio < prioridade_max)
				prioridade_max = prio;
		}
		else
		{
			printf("Valor de prioridade não aceito");
		}
	}
	else
	{
		atual->priority = prio;
	}
}

int task_getprio(task_t *task)
{
	if (task != NULL)
		return (task->priority);
	else
		return (atual->priority);
}


// suspende UMA tarefa, retirando-a de sua fila atual, adicionando-a à fila
// queue e mudando seu estado para "suspensa"; usa a tarefa atual se task==NULL
void task_suspend(task_t *task, task_t **queue)
{
	zona_crit = 1;
	if( task == NULL && atual != NULL )
	{
		queue_remove((queue_t **)&fila_prontas, (queue_t *)(atual));
		queue_append((queue_t **)queue, (queue_t *)(atual));
		atual->pronta = 0;
	}
	else
	{	
		queue_remove((queue_t **)&fila_prontas, (queue_t*)(task));
		queue_append((queue_t **)&fila_suspensas, (queue_t*)(task));
		task->pronta = 0;
	}
	zona_crit =0;
}

// acorda UMA tarefa, retirando-a de sua fila atual, adicionando-a à fila de
// tarefas prontas ("ready queue") e mudando seu estado para "pronta"
void task_resume(task_t *task)
{
	if( task != NULL )
	{
		zona_crit = 1;
		task->pronta = 1;
		queue_remove((queue_t **)&fila_suspensas, (queue_t *)(task));
		queue_append((queue_t **)&fila_prontas, (queue_t *)(task));
		zona_crit = 0;
	}
}

// a tarefa corrente aguarda o encerramento de outra task
int task_join (task_t *task)
{
	if( task == NULL )
		return(-1);
	
	atual->task_suspend = task->id; // id da tarefa que a suspendeu
	task_suspend(NULL,(task_t**)&fila_suspensas); // insere na fila de suspensas a atual
//	task_yield();	
	
	//printf("O problema ta no switch\n");
	task_switch(&task_dispatcher);
	//printf("Depois do switch\n");

	task_t* aux = (task_t *)fila_suspensas; // retornando as tarefas que foram suspensas para a fila de prontas
	task_t* aux2 = aux->next;
//	printf("Ta aqui\n");	
	while(fila_suspensas != NULL) // enquanto não esvaziar a fila de suspensas
	{
		if( aux != NULL )
			aux2 = aux2->next;
		if( task->id == aux->task_suspend ) // aux estava esperado atual finalizar
		{
			aux->pronta = 1;
			task_resume(aux); // acordada, volta para a fila de prontas
		}
		if( aux2!=NULL )
			aux = aux2;
		if( aux == (task_t *)fila_suspensas )//voltou para o começo
			break;
	}
	return(exit_Code);
}


// suspende a tarefa corrente por t segundos: tira a tarefa corrente da fila de prontas, a coloca na fila de tarefas adormecidas e devolce o controle ao dispatcher
void task_sleep (int t)
{
	zona_crit = 1;
    // inserindo na struct da tarfa o instante em que ela deve ser acordada = tempo atual + t
      atual->task_wake = (int)( systime() + t*1000 );
    // removendo da fila de prontas e colocando na fila de adormecidas
	queue_remove((queue_t **)&fila_prontas, (queue_t *)(atual));
	queue_append((queue_t **)&fila_adormecidas, (queue_t *)(atual));
    // devolvendo o controle ao dispatcher
    zona_crit = 0;
    task_switch(&task_dispatcher);
}


// PROJETO 10 SEMÁFOROS =================================================================================================================================================

// cria um semáforo com valor inicial "value"
int sem_create (semaphore_t *s, int value)
{
	if( s == NULL )
		return(-1); // fracasso
	s->fila_semaforo = NULL;
	s->count = value; 
	s->ativo = 1; // semáforo ativo, pronto para uso e não destruído
	return(0); // sucesso
}

// requisita o semáforo
int sem_down (semaphore_t *s){
	if ( s == NULL || s->ativo == 0 ) // semáforo inexistente ou destruído
		return(-1); // derrota

	zona_crit = 1;
	s->count = s->count -1;
	
	if ( s->count < 0)
	{
//		printf("%d",s->count);
		//printf("Systime = %d, Down-",(int)systime());
//		printf("%d\n",atual->id);
	//	check_Q((queue_t**)&(s->fila_semaforo));
		task_suspend(NULL,&(s->fila_semaforo));
		zona_crit = 0;
		task_yield();
	}
	zona_crit = 0;
	return(0); // sucesso
}

// libera o semáforo
int sem_up (semaphore_t *s)
{
	//check_Q(&fila_prontas);
	if ( s == NULL ) // semáforo inexistente ou destruído
		return(-1); // derrota
	zona_crit = 1;
	s->count= s->count + 1;
	if ( s->count <= 0 ) // a fila do semáforo não está vazia
	{
//		printf("Up-");
		s->fila_semaforo->pronta = 1;
		task_t* tempt = s->fila_semaforo;
		queue_append((queue_t **)(fila_prontas), queue_remove((queue_t **)&(s->fila_semaforo),(queue_t *)tempt));
		tempt = (task_t*)fila_prontas->prev;
		//printf("next:%d\n",tempt->next->id);	
	//	check_Q(&fila_prontas);
			
	}
	zona_crit = 0;
	if ( s->ativo == 0 )
		return(-1); // tarefa retorna da operação dawn com -1 porque o semaforo sera destruido
	return(0); // sucesso
}

// destroi o semáforo, liberando as tarefas bloqueadas
int sem_destroy (semaphore_t *s)
{
	if ( s == NULL )
		return(-1);
	zona_crit =1;
	s->ativo = 0;
	while ( s->count <= 0 ) // liberando todas as tarefas do semáforo
		sem_up(s);
	s = NULL; // destruindo o semáforo
	zona_crit = 0;
	return(0);
}

// barreiras /////////////////////////////////////////////////////////////////////////////////////////////////

// Inicializa uma barreira
int barrier_create (barrier_t *b, int N) {
	if( b == NULL || N <= 0 )
		return(-1);
	sem_create(&b->sem,1);
	b->ativo = 1;
	b->N = N;
	return(0);
}

// Chega a uma barreira
int barrier_join (barrier_t *b) {
	if( b == NULL || b->ativo == 0 ) // barreira inexistente ou destruída
		return(-1);
	sem_down(&b->sem);
	queue_remove((queue_t**)&(fila_prontas),(queue_t*)(atual));
	queue_append((queue_t **)&(b->fila_barreira),(queue_t*)(atual));
	int bar_tasks = (int)queue_size((queue_t*)(b->fila_barreira));
	if( bar_tasks >= b->N ) // hora de destruir a barreira
		barrier_destroy(b);
	sem_up(&b->sem);
	if( b->ativo == 0 ) {// o semáforo foi destruído no meio do caminho
		b->ativo = 1;
		b->fila_barreira = NULL;
		return(-1);
	}
	task_switch(&task_dispatcher); // devolve o controle ao dispatcher
	return(0);
}

// Destrói uma barreira
int barrier_destroy (barrier_t *b) {
	if( b == NULL || b->ativo == 0 ) // barreira inexistente ou destruída
		return(-1);
	b->ativo = 0;
	task_t* t_b = NULL;
	int bar_tasks = (int)queue_size((queue_t *)(b->fila_barreira));
	while( bar_tasks > 0 )
	{	
		t_b = b->fila_barreira;
		if(t_b->id == 0 && bar_tasks>1)
		{
			queue_remove((queue_t **)&(b->fila_barreira), (queue_t *)(t_b));
			queue_append((queue_t **)&(b->fila_barreira), (queue_t *)t_b);	
		}else
		{
			queue_remove((queue_t **)&(b->fila_barreira), (queue_t *)(t_b));
			queue_append((queue_t **)&fila_prontas, (queue_t *)t_b);
			bar_tasks--;
		}
		
	}
	sem_destroy(&b->sem);
	return(0);
}

// mensagens /////////////////////////////////////////////////////////////////////////////////////////////////

int mqueue_create(mqueue_t* queue, int max, int size)
{
	if( queue == NULL || max <= 0 || size <= 0 )
		return(-1);
	sem_create(&sem_access,1);
	sem_create(&sem_buffer,max);
	queue->max = max;
	queue->n_msg = 0;
	queue->size = size;
	queue->msg_queue = malloc(size*max);
	return(0);
}

int mqueue_send(mqueue_t* queue, void *msg)
{
	if(sem_access.ativo == 0 || sem_buffer.ativo == 0|| msg == NULL)
		return(-1);
	sem_down(&sem_buffer);

	sem_down(&sem_access);
	
	bcopy(msg, (&queue->msg_queue[queue->n_msg*queue->size]),queue->size);
	queue->n_msg++;

	sem_up(&sem_access);
	return(0);
}

int mqueue_recv(mqueue_t* queue, void *msg)
{
	
	while(queue->n_msg == 0 )
	{
		task_yield();
		if(sem_access.ativo == 0 || sem_buffer.ativo == 0 ||msg==NULL)
		{	
			return(-1);
		}
	}	
	if(sem_access.ativo == 0 || sem_buffer.ativo == 0 ||msg==NULL)
	{
		return(-1);
	}
	sem_down(&sem_access);
	bcopy(&queue->msg_queue[0],msg,queue->size);
	queue->n_msg--;
		bcopy(&queue->msg_queue[queue->size],&queue->msg_queue[0],queue->size*queue->n_msg);

	sem_up(&sem_access);
	sem_up(&sem_buffer);
	return(0);
}

int mqueue_destroy (mqueue_t* queue)
{
	if(queue==NULL)
		return(-1);
	free(queue->msg_queue);
	queue->max = 0;
	queue->n_msg = 0;
	queue->size = 0;
	sem_destroy(&sem_access);
	sem_destroy(&sem_buffer);
	return(0);
}

int mqueue_msgs(mqueue_t *queue)
{
	if(queue == NULL)
		return(-1);
	else
		return(queue->n_msg);
}


