#include<stdio.h>
#include<stdlib.h>
#include "pingpong.h"

task_t  p1,p2,p3,c1,c2;
semaphore_t s_vaga, s_buffer, s_item;


typedef struct filaint_t //Utilizado pelo professor no p00
{
	struct filaint_t *next;
	int id;
} filaint_t;
filaint_t* buffer = NULL;
int item;

void insert(int val)
{
	filaint_t* new = malloc(sizeof(filaint_t));
	new->id = val;
	new->next = buffer;
	buffer = new;
}

void remove_fila(void *arg)
{
	filaint_t* aux =NULL;
	if(buffer!=NULL)
	{
		aux = buffer;
		filaint_t* ant=NULL;
		while(aux->next !=NULL)
		{
			ant = aux;
			aux = aux->next;
		}
		
		if(ant!=NULL)
			ant->next = NULL;
		else
			buffer = NULL;

		printf("%s consumiu %d\n",(char*) arg, aux->id);
		free(aux);

	}
	else
		printf("sem items no buffer\n");
}

void produtor(void * arg)
{
	while (1)
	{
		
		task_sleep (1);
		item = rand() % 100;
		sem_down(&s_vaga);
		printf("%s produziu %d\n",(char*) arg, item);
		sem_down(&s_buffer);
		insert(item);
		sem_up(&s_buffer);
		sem_up(&s_item);
	}
}

void consumidor(void * arg)
{
	while(1)
	{
		
	//	printf("estou em %s\n",(char*) arg);
		sem_down(&s_item);
	//	printf("estou em %s\n",(char*) arg);
		sem_down(&s_buffer);
		remove_fila(arg);
		sem_up(&s_buffer);
		sem_up(&s_vaga);

		//print item
		task_sleep(1);
		
//			printf("estou em %s\n",(char*) arg);
	}
}

int main (int argc, char *arv[])
{
	printf("Main INICIO\n");

	pingpong_init();

	sem_create (&s_vaga, 5);
	sem_create(&s_buffer,1);
	sem_create(&s_item,0);

	task_create (&p1, produtor, "P1");
	task_create (&p2, produtor, "P2");
	task_create (&p3, produtor, "P3");
	task_create (&c1, consumidor, "		C1");
	task_create (&c2, consumidor, "		C2");
	
	task_join(&p1);

	printf("Main FIM\n");
	task_exit(0);

	exit(0);
	

}
