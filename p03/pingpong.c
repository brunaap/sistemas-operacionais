#include "pingpong.h"

// funções gerais ==============================================================
//#define DEBUG;
#define STACKSIZE 32768		/* tamanho de pilha das threads */
int id_task;
int id_atual;
int userTasks;
task_t *atual;
task_t main_t;
task_t task_dispatcher;
queue_t *fila_prontas;
ucontext_t main_c;


// decide a proxima tareda que sera ativada. FCFS, a primeira que entra e a primeira que sai
task_t* scheduler()
{
	if(fila_prontas)
	{
		task_t* executar_agora = (task_t*)fila_prontas;
    	return executar_agora;
	}
	else
		return NULL;
}

// corpo do dispatcher
void dispatcher_body ()
{
	userTasks = (int)queue_size((queue_t*)fila_prontas); // pega o numero de tarefas prontas
	task_t *next = NULL;
	while ( userTasks > 0 ) // enquanto tiverem tarefas prontas na fila
	{
		next = scheduler() ; // scheduler é uma função
		if(next)
		{
			queue_remove((queue_t**)&fila_prontas, (queue_t*)next); // tirando da fila
			queue_append((queue_t**)&fila_prontas, (queue_t*)next) ;
			task_switch(next) ; // transfere controle para a tarefa "next"
		}
	}
	task_exit(0) ; // encerra a tarefa dispatcher
}

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init()
{
	id_task = 0;
	id_atual = 0;
    
    main_t.context = &main_c;
    main_t.id = id_task;
    id_task+=1;
	atual = &main_t;

	task_create(&task_dispatcher,(void*)(dispatcher_body), "dispatcher"); // criando dispatcher
	task_dispatcher.id = id_task;
	id_task+=1;

	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf(stdout, 0, _IONBF, 0);
}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create(task_t *task,			// descritor da nova tarefa
	void(*start_func)(void *),	// funcao corpo da tarefa
	void *arg)			// argumentos para a tarefa
{
	ucontext_t* context = malloc(sizeof(ucontext_t));
	task->context = context;
	char *stack = NULL;

	getcontext(context);

	stack = malloc(STACKSIZE);
	if (stack)
	{
		context->uc_stack.ss_sp = stack;
		context->uc_stack.ss_size = STACKSIZE;
		context->uc_stack.ss_flags = 0;
		context->uc_link = 0;
	}
	else
	{
		perror("Erro na criação da pilha: ");
		return(-1);
	}

	makecontext(context, (void*)(*start_func), 1, arg);

	task->id = id_task;
    #ifdef DEBUG
    printf("Criou tarefa %d \n", id_task);
    #endif
	id_task += 1;

	task->pronta = 1;
	if(task->id>1)
	{
		queue_append((queue_t**)&fila_prontas,(queue_t*)(task));
		//task->fila = (queue_t**)&pronta;
	}

	return(id_task);
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit(int exitCode)
{
	userTasks--;
	if(fila_prontas)
		queue_remove(&fila_prontas, (queue_t*)atual);
	if(id_atual==1) // ta no dispatcher
		task_switch(&main_t);
	else // está na main
		task_switch(&task_dispatcher);
}

// alterna a execução para a tarefa indicada
int task_switch(task_t *task)
{
    
    if(atual != NULL && task!=NULL && atual->context != NULL && task->context!=NULL)
    {
        #ifdef DEBUG
        printf("Mudar de tarefa %d para tarefa %d \n",atual->id, task->id); 
        #endif
        ucontext_t* origem;
        origem = atual->context;
        atual = task;
        id_atual = task->id;
        swapcontext(origem, task->context);
        
    }
    else
	{
        return (-1);
	}
    
    return 0;
}

// permite a uma tarefa voltar ao final da fila de prontas
void task_yield (task_t *task)
{
    #ifdef DEBUG
    printf("Mudar de tarefa %d para tarefa %d \n",atual->id, task->id); 
    #endif
	task_switch(&task_dispatcher);  
}

// retorna o identificador da tarefa corrente (main eh 0)
int task_id()
{
	return(id_atual);
}

// suspende uma tarefa, retirando-a de sua fila atual, adicionando-a à fila
// queue e mudando seu estado para "suspensa"; usa a tarefa atual se task==NULL
void task_suspend(task_t *task, task_t **queue);

// acorda uma tarefa, retirando-a de sua fila atual, adicionando-a à fila de
// tarefas prontas ("ready queue") e mudando seu estado para "pronta"
void task_resume(task_t *task);