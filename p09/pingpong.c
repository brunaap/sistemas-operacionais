#include "pingpong.h"

// funções gerais ==============================================================
//#define DEBUG
#define STACKSIZE 32768 /* tamanho de pilha das threads */
int id_task;
int id_atual;

int userTasks;
int sleepTasks;
int prioridade_max;
int task_aging = 1;

int quantum_atual = 0;
int quantum = 11;
int ticks = 0;
int zona_crit = 0;
int exit_Code = -10; 

task_t *atual;
task_t main_t;
task_t task_dispatcher; 
queue_t *fila_prontas = NULL;              // fila de tarefas printas
queue_t *fila_suspensas = NULL;            // fila de tarefas suspensaas
queue_t *fila_adormecidas = NULL;          // fila de tarefas adormecidas

struct sigaction action;
// estrutura de inicialização to timer
struct itimerval timer;


//informar às tarefas o valor corrente do relógio, numero de ticks decorridos desde a inicialização do programa (1 tick = 1ms)
unsigned int systime ()
{
	return ticks;
}


void ticks_handler()
{
	ticks++;
	if (atual->user_task && zona_crit ==0)
	{
		quantum_atual = quantum_atual - 1;
		if (quantum_atual <= 0)
			task_yield();
	}
}

// decide a proxima tareda que sera ativada. utiliza prioridades
task_t *scheduler()
{
	if (fila_prontas)
	{
		task_t *executar_agora = (task_t *)fila_prontas;
		task_t *aux = (task_t *)fila_prontas;

		prioridade_max = task_getprio(aux);

		for (aux = aux->next; aux != (task_t *)fila_prontas; aux = aux->next)
		{
			if (prioridade_max > task_getprio(aux))
			{
				prioridade_max = task_getprio(aux);
			}
		}
		#ifdef DEBUG
				printf("Prioridade máxima: %d,Tarefa: %d, Prioridade da tarefa: %d \n", prioridade_max, executar_agora->id, task_getprio(executar_agora));
		#endif
		while (task_getprio(executar_agora) > prioridade_max)
		{
			queue_remove((queue_t **)&fila_prontas, (queue_t *)executar_agora); // tirando da fila
			queue_append((queue_t **)&fila_prontas, (queue_t *)executar_agora);
			executar_agora = (task_t *)fila_prontas;
		}
		return executar_agora;
	}
	else
		return NULL;
}

// corpo do dispatcher
void dispatcher_body()
{
    // percorrendo a fila de prontas e a organizando
	userTasks = (int)queue_size((queue_t *)fila_prontas); // pega o numero de tarefas prontas
	task_t *next = NULL;
	while (userTasks > 0)
	{
        // percorrendo a fila de tarefas adormecidas e acordando as que o tempo estorou
		if( fila_adormecidas )
		{
			task_t *tarefa_sleep = (task_t *)fila_adormecidas;
			task_t *tarefa_first = (task_t *)fila_adormecidas;
			int time = systime();
			//sleepTasks = (int)queue_size((queue_t *)fila_adormecidas);
			do
			{
				task_t *temp = tarefa_sleep->next;
				if ( tarefa_sleep->task_wake <= time ) // a tarefa deveser acordada
				{
					// removendo da fila de adormecidas e colocando na fila de prontas
					queue_remove((queue_t **)&fila_adormecidas, (queue_t *)(tarefa_sleep)); 
					queue_append((queue_t **)&fila_prontas, (queue_t *)(tarefa_sleep));

					tarefa_first = (task_t *)fila_adormecidas;
					//sleepTasks--;
				}
				
				tarefa_sleep = temp;
			} while (tarefa_sleep != NULL && tarefa_sleep != tarefa_first);
			
		}
        
        // voltando às funções usuais do dispatcher
		next = scheduler(); // scheduler é uma função
		if (next)
		{
			next->tempo_entrou_na_tarefa = systime(); // tafeva vai ser executada, é preciso computadar o tempo de processamento dela
			task_switch(next); // transfere controle para a tarefa "next"
		}
	}
    
	task_exit(0); // encerra a tarefa dispatcher
}

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void pingpong_init()
{
	id_task = 0;
	id_atual = 0;
	prioridade_max = 20; // THE END
	main_t.context = malloc(sizeof(ucontext_t));
	char *stack = NULL;

	getcontext(main_t.context);

	stack = malloc(STACKSIZE);
	if (stack)
	{
		main_t.context->uc_stack.ss_sp = stack;
		main_t.context->uc_stack.ss_size = STACKSIZE;
		main_t.context->uc_stack.ss_flags = 0;
		main_t.context->uc_link = 0;
	}
	else
	{
		perror("Erro na criação da pilha: ");
	}


	//TIMER -----------------------------------------------------------------------------------------
	action.sa_handler = ticks_handler;
	sigemptyset(&action.sa_mask);
	action.sa_flags = 0;

	if (sigaction(SIGALRM, &action, 0) < 0)
	{
		perror("Erro em sigaction: ");
		exit(1);
	}
	timer.it_value.tv_usec = 1;		  // primeiro disparo, em micro-segundos
	timer.it_value.tv_sec = 0;		  // primeiro disparo, em segundos
	timer.it_interval.tv_usec = 1; // disparos subsequentes, em micro-segundos
	timer.it_interval.tv_sec = 0;	 // disparos subsequentes, em segundos
	//END TIMER -----------------------------------------------------------------------------------------

	id_task = 1;
	task_create(&task_dispatcher, (void *)(dispatcher_body), "dispatcher"); // criando dispatcher
	id_task = 0;
	task_dispatcher.user_task = 0;
	quantum_atual = quantum;
	if (setitimer(ITIMER_REAL, &timer, 0) < 0)
	{
		perror("Erro em setitimer: ");
		exit(1);
	}
	
	//INICIAR A MAIN --------------------------------------------------------
	main_t.id = id_task;
	main_t.user_task = 1;
	id_task = 2;
	atual = &main_t;
	queue_append((queue_t**)&fila_prontas, (queue_t*)(&main_t));
	main_t.tempo_inicio = systime();
	main_t.tempo_execucao = 0;
  	main_t.tempo_processador = 0;
  	main_t.activations = 0;
	main_t.tempo_entrou_na_tarefa = 0;

	/* desativa o buffer da saida padrao (stdout), usado pela função printf */
	setvbuf(stdout, 0, _IONBF, 0);
}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create(task_t *task,				// descritor da nova tarefa
				void (*start_func)(void *), // funcao corpo da tarefa
				void *arg)					// argumentos para a tarefa
{
	ucontext_t *context = malloc(sizeof(ucontext_t));
	task->user_task = 1;
	task->context = context;
	char *stack = NULL;

	getcontext(context);

	stack = malloc(STACKSIZE);
	if (stack)
	{
		context->uc_stack.ss_sp = stack;
		context->uc_stack.ss_size = STACKSIZE;
		context->uc_stack.ss_flags = 0;
		context->uc_link = 0;
	}
	else
	{
		perror("Erro na criação da pilha: ");
		return (-1);
	}

	makecontext(context, (void *)(*start_func), 1, arg);

	task->id = id_task;
	#ifdef DEBUG
		printf("Criou tarefa %d \n", id_task);
	#endif

	if (task->id > 1)
	{
		queue_append((queue_t **)&fila_prontas, (queue_t *)(task));
	}
	id_task += 1;
    task->task_wake = -10;  // a tarefa ainda não está dormindo
	task->task_suspend = -10; // tarefa que deve terminar para ela voltar
	task->pronta = 1;
	task->tempo_inicio = systime();
	task->tempo_execucao = 0;
  	task->tempo_processador = 0;
  	task->activations = 0;
	task->tempo_entrou_na_tarefa = 0;
	return (id_task);
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit(int exitCode)
{
	exit_Code = exitCode;
	//tarefa saidno, retirna o tempo de execução dela
	atual->tempo_execucao = systime() - (atual->tempo_inicio);
	printf("Task %d exit: execution time %d ms, processor time %d ms, %d activations\n", atual->id, atual->tempo_execucao, atual->tempo_processador, atual->activations);
	atual->id = -2;
	userTasks--;
	if (fila_prontas)
		queue_remove(&fila_prontas, (queue_t *)atual);
	if (id_atual == 1) // ta no dispatcher
		task_switch(&main_t);
	else // pode estar em qualquer uma menos a dispatcher
	{
		if( atual->id == 0 ) // está na main
			task_switch(&task_dispatcher);
		else // não está na main
			task_switch(&main_t);
	}
		
}

// alterna a execução para a tarefa indicada
int task_switch(task_t *task)
{
	zona_crit = 1;
	if (atual != NULL && task != NULL && atual->context != NULL && task->context != NULL)
	{
		#ifdef DEBUG
			printf("Mudar de tarefa %d para tarefa %d \n", atual->id, task->id);
		#endif
		ucontext_t *origem;
		origem = atual->context;
		atual = task;
		id_atual = task->id;
		quantum_atual = quantum;
		swapcontext(origem, task->context);
		//printf("Saiu de boa\n");
		zona_crit = 0;
		return(0);
	}
	return(-1);
}

// permite a uma tarefa voltar ao final da fila de prontas
void task_yield()
{	
	zona_crit =1;
	// se entra aqui vai para o dispatcher
	(&task_dispatcher)->activations++;

	atual->tempo_processador+=(systime())-(atual->tempo_entrou_na_tarefa); // adicionando o tempo de uso do processador
	(atual->activations)++; // se voltou para a fila de prontas é porque foi ativada uma vez
	quantum_atual = quantum;
	//task_t *executar_agora = (task_t *)fila_prontas;
	//executar_agora->priority = executar_agora->priority + task_aging;
	zona_crit =0;
	task_switch(&task_dispatcher);
}

// retorna o identificador da tarefa corrente (main eh 0)
int task_id()
{
	return (id_atual);
}

void task_setprio(task_t *task, int prio)
{
	if (task != NULL)
	{
		if (prio >= -20 && prio <= 20)
		{
			task->priority = prio;
			if (prio < prioridade_max)
				prioridade_max = prio;
		}
		else
		{
			printf("Valor de prioridade não aceito");
		}
	}
	else
	{
		atual->priority = prio;
	}
}

int task_getprio(task_t *task)
{
	if (task != NULL)
		return (task->priority);
	else
		return (atual->priority);
}


// suspende UMA tarefa, retirando-a de sua fila atual, adicionando-a à fila
// queue e mudando seu estado para "suspensa"; usa a tarefa atual se task==NULL
void task_suspend(task_t *task, task_t **queue)
{
	zona_crit = 1;
	if( task == NULL && atual != NULL )
	{
	
		queue_append((queue_t **)queue, queue_remove((queue_t **)&fila_prontas, (queue_t *)(atual)));
		atual->pronta = 0;
	}
	else
	{	
		queue_remove((queue_t **)&fila_prontas, (queue_t*)(task));
		queue_append((queue_t **)&fila_suspensas, (queue_t*)(task));
		task->pronta = 0;
	}
	zona_crit =0;
}

// acorda UMA tarefa, retirando-a de sua fila atual, adicionando-a à fila de
// tarefas prontas ("ready queue") e mudando seu estado para "pronta"
void task_resume(task_t *task)
{
	zona_crit = 1;
	if( task != NULL )
	{
		task->pronta = 1;
		queue_append((queue_t **)&fila_prontas, queue_remove((queue_t **)&fila_suspensas, (queue_t *)(task)));
	}
	zona_crit = 0;
}

// a tarefa corrente aguarda o encerramento de outra task
int task_join (task_t *task)
{
	if( task == NULL )
		return(-1);

	if(task->id == -2)
		return (-1);
	

	atual->task_suspend = task->id; // id da tarefa que a suspendeu
	task_suspend(NULL,(task_t**)&fila_suspensas); // insere na fila de suspensas a atual
	task_yield();	
	
	//printf("O problema ta no switch\n");
	//task_switch(&task_dispatcher);
	//printf("Depois do switch\n");

	task_t* aux = (task_t *)fila_suspensas; // retornando as tarefas que foram suspensas para a fila de prontas
	task_t* aux2 = aux->next;
	//printf("Ta aqui\n");
	while( (task_t *)fila_suspensas != NULL ) // enquanto não esvaziar a fila de suspensas
	{
		if( aux != NULL )
			aux2 = aux2->next;
		if( task->id == aux->task_suspend ) // aux estava esperado atual finalizar
		{
			aux->pronta = 1;
			task_resume(aux); // acordada, volta para a fila de prontas
		}
		if( aux2!=NULL )
			aux = aux2;
		if( aux == (task_t *)fila_suspensas )//voltou para o começo
			break;
	}
	return(exit_Code);
}


// suspende a tarefa corrente por t segundos: tira a tarefa corrente da fila de prontas, a coloca na fila de tarefas adormecidas e devolce o controle ao dispatcher
void task_sleep (int t)
{
    // inserindo na struct da tarfa o instante em que ela deve ser acordada = tempo atual + t
    atual->task_wake = (int)( systime() + t*1000 );
    // removendo da fila de prontas e colocando na fila de adormecidas
	queue_append((queue_t **)&fila_adormecidas, queue_remove((queue_t **)&fila_prontas, (queue_t *)(atual)));
    // devolvendo o controle ao dispatcher
    //task_switch(&task_dispatcher);
    task_yield();
}
