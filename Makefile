run_p00:
	gcc -c p00/queue.c -o p00/queue.o -Wall; \
	ar rvs p00/lib.a p00/queue.o; \
	gcc -o p00/executavel p00/testafila.c -L. p00/lib.a -Wall; \
	p00/./executavel -Wall; \

run_p01:
	gcc -o p01/executavel p01/contexts.c -Wall; \
	p01/./executavel -Wall; \

run_p02_tasks1:
	gcc -c p02/queue.c -o p02/queue.o -Wall; \
	gcc -c p02/pingpong.c -o p02/pingpong.o -Wall; \
	ar rvs p02/lib.a p02/queue.o p02/pingpong.o; \
	gcc -o p02/executavel-tasks1 p02/pingpong-tasks1.c -L. p02/lib.a -Wall; \
	p02/./executavel-tasks1 -Wall; \

run_p02_tasks2:
	gcc -c p02/queue.c -o p02/queue.o -Wall; \
	gcc -c p02/pingpong.c -o p02/pingpong.o -Wall; \
	ar rvs p02/lib.a p02/queue.o p02/pingpong.o; \
	gcc -o p02/executavel-tasks2 p02/pingpong-tasks2.c -L. p02/lib.a -Wall; \
	p02/./executavel-tasks2 -Wall; \

run_p02_tasks3:
	gcc -c p02/queue.c -o p02/queue.o -Wall; \
	gcc -c p02/pingpong.c -o p02/pingpong.o -Wall; \
	ar rvs p02/lib.a p02/queue.o p02/pingpong.o; \
	gcc -o p02/executavel-tasks3 p02/pingpong-tasks3.c -L. p02/lib.a -Wall; \
	p02/./executavel-tasks3 -Wall; \

run_p03:
	gcc -c p03/queue.c -o p03/queue.o -Wall; \
	gcc -c p03/pingpong.c -o p03/pingpong.o -Wall; \
	ar rvs p03/lib.a p03/queue.o p03/pingpong.o; \
	gcc -o p03/executavel p03/pingpong-dispatcher.c -L. p03/lib.a -Wall; \
	p03/./executavel -Wall; \

run_p04:
	gcc -c p04/queue.c -o p04/queue.o -Wall; \
	gcc -c p04/pingpong.c -o p04/pingpong.o -Wall; \
	ar rvs p04/lib.a p04/queue.o p04/pingpong.o; \
	gcc -o p04/executavel p04/pingpong-scheduler.c -L. p04/lib.a -Wall; \
	p04/./executavel -Wall; \

run_p05:
	gcc -c p05/queue.c -o p05/queue.o -Wall; \
	gcc -c p05/pingpong.c -o p05/pingpong.o -Wall; \
	ar rvs p05/lib.a p05/queue.o p05/pingpong.o; \
	gcc -o p05/executavel p05/pingpong-preempcao.c -L. p05/lib.a -Wall; \
	p05/./executavel -Wall; \

run_p06_contab:
	gcc -c p06/queue.c -o p06/queue.o -Wall; \
	gcc -c p06/pingpong.c -o p06/pingpong.o -Wall; \
	ar rvs p06/lib.a p06/queue.o p06/pingpong.o; \
	gcc -o p06/executavel-contab p06/pingpong-contab.c -L. p06/lib.a -Wall; \
	p06/./executavel-contab -Wall; \

run_p06_contab-prio:
	gcc -c p06/queue.c -o p06/queue.o -Wall; \
	gcc -c p06/pingpong.c -o p06/pingpong.o -Wall; \
	ar rvs p06/lib.a p06/queue.o p06/pingpong.o; \
	gcc -o p06/executavel-contab-prio p06/pingpong-contab-prio.c -L. p06/lib.a -Wall; \
	p06/./executavel-contab-prio -Wall; \

run_p07:
	gcc -c p07/queue.c -o p07/queue.o -Wall; \
	gcc -c p07/pingpong.c -o p07/pingpong.o -Wall; \
	ar rvs p07/lib.a p07/queue.o p07/pingpong.o; \
	gcc -o p07/pingpong-maintask p07/pingpong-maintask.c -L. p07/lib.a -Wall; \
	p07/./pingpong-maintask -Wall; \

run_p08:
	gcc -c p08/queue.c -o p08/queue.o -Wall; \
	gcc -c p08/pingpong.c -o p08/pingpong.o -Wall; \
	ar rvs p08/lib.a p08/queue.o p08/pingpong.o; \
	gcc -o p08/pingpong-join p08/pingpong-join.c -L. p08/lib.a -Wall; \
	p08/./pingpong-join -Wall; \

run_p09:
	gcc -c p09/queue.c -o p09/queue.o -Wall; \
	gcc -c p09/pingpong.c -o p09/pingpong.o -Wall; \
	ar rvs p09/lib.a p09/queue.o p09/pingpong.o; \
	gcc -o p09/pingpong-sleep p09/pingpong-sleep.c -L. p09/lib.a -Wall; \
	p09/./pingpong-sleep -Wall; \

run_p10_racecond:
	gcc -c p10/queue.c -o p10/queue.o -Wall; \
	gcc -c p10/pingpong.c -o p10/pingpong.o -Wall; \
	ar rvs p10/lib.a p10/queue.o p10/pingpong.o; \
	gcc -o p10/pingpong-racecond p10/pingpong-racecond.c -L. p10/lib.a -Wall; \
	p10/./pingpong-racecond -Wall; \

run_p10_semaphore:
	gcc -c p10/queue.c -o p10/queue.o -Wall; \
	gcc -c p10/pingpong.c -o p10/pingpong.o -Wall; \
	ar rvs p10/lib.a p10/queue.o p10/pingpong.o; \
	gcc -o p10/pingpong-semaphore p10/pingpong-semaphore.c -L. p10/lib.a -Wall; \
	p10/./pingpong-semaphore -Wall; \

run_p10_prodcons:
	gcc -c p10/queue.c -o p10/queue.o -Wall; \
	gcc -c p10/pingpong.c -o p10/pingpong.o -Wall; \
	ar rvs p10/lib.a p10/queue.o p10/pingpong.o; \
	gcc -o p10/pingpong-prodcons p10/pingpong-prodcons.c -L. p10/lib.a -Wall; \
	p10/./pingpong-prodcons -Wall; \

run_p11:
	gcc -c p11/queue.c -o p11/queue.o -Wall; \
	gcc -c p11/pingpong.c -o p11/pingpong.o -Wall; \
	ar rvs p11/lib.a p11/queue.o p11/pingpong.o; \
	gcc -o p11/pingpong-barrier p11/pingpong-barrier.c -L. p11/lib.a -Wall; \
	p11/./pingpong-barrier -Wall; \
	
run_p12:
	gcc -c p12/queue.c -o p12/queue.o -Wall; \
	gcc -c p12/pingpong.c -o p12/pingpong.o -Wall; \
	ar rvs p12/lib.a p12/queue.o p12/pingpong.o; \
	gcc -o p12/pingpong-mqueue p12/pingpong-mqueue.c -lm -L. p12/lib.a -Wall; \
	p12/./pingpong-mqueue -Wall; \

