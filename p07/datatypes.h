// PingPongOS - PingPong Operating System
// Prof. Carlos A. Maziero, DAINF UTFPR
// Versão 1.0 -- Março de 2015
//
// Estruturas de dados internas do sistema operacional

#ifndef __DATATYPES__
#define __DATATYPES__
#include <ucontext.h>
#include <signal.h>
#include <sys/time.h>

// Estrutura que define uma tarefa
typedef struct task_t
{
  struct task_t *prev, *next; //filas
  int id; // id da task
  ucontext_t* context;
  void *stack;
  struct task_t *parent;
  int priority;
  int user_task; // 0 = system task
  unsigned int tempo_execucao;
  unsigned int tempo_processador;
  unsigned int tempo_inicio;
  unsigned int tempo_entrou_na_tarefa;
  int activations;
} task_t ;

// estrutura que define um semáforo
typedef struct
{
  // preencher quando necessário
} semaphore_t ;

// estrutura que define um mutex
typedef struct
{
  // preencher quando necessário
} mutex_t ;

// estrutura que define uma barreira
typedef struct
{
  // preencher quando necessário
} barrier_t ;

// estrutura que define uma fila de mensagens
typedef struct
{
  // preencher quando necessário
} mqueue_t ;

#endif
